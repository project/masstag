<?php

/**
 * @file
 * Tag all nodes containing a keyword or phrase.
 */

/**
 * Render the initial and confirmation forms.
 */
function masstag_form(&$form_state) {
  $form = array();
  if ($form_state['values']['tid']) {
    $search_query = search_parse_query('"' . $form_state['values']['key'] . '"');
    $result = NULL;
    switch ($form_state['values']['handling']) {
      case 'all':
        if ($search_query[2]) {
          $query = "SELECT n.nid, n.vid, n.title FROM {node} n INNER JOIN {search_index} i ON n.nid = i.sid AND i.type = 'node' WHERE " . $search_query[2] . " GROUP BY i.type, i.sid HAVING COUNT(*) >= %d";
          $result = db_query($query, array_merge(array_values($search_query[3]), array($search_query[4])));
        }
        break;
      case 'any':
        if ($search_query[2]) {
          $query = "SELECT n.nid, n.vid, n.title FROM {node} n INNER JOIN {search_index} i ON n.nid = i.sid AND i.type = 'node' WHERE " . $search_query[2] . " GROUP BY i.type, i.sid";
          $result = db_query($query, array_values($search_query[3]));
        }
        break;
      case 'exact':
        if ($search_query[0]) {
          $query = "SELECT n.nid, n.vid, n.title FROM {node} n INNER JOIN {search_dataset} d ON n.nid = d.sid AND d.type = 'node' WHERE " . $search_query[0];
          $result = db_query($query, $search_query[1]);
        }
        break;
    }
    $form['nodes'] = array();
    while ($node = db_fetch_object($result)) {
      $form['nodes'][$node->nid] = array('#type' => 'hidden', '#value' => $node->vid);
      $nodes[] = l($node->title, 'node/' . $node->nid);
    }
    if ($form['nodes']) {
      $term = taxonomy_get_term($form_state['values']['tid']);
      $form['term'] = array(
        '#type' => 'value',
        '#value' => $term,
      );
      $form['nodes']['#tree'] = TRUE;
      $form['nodes']['#prefix'] = theme('item_list', $nodes);
      $description = format_plural(count($nodes), 'This post will be tagged with the term !term.', 'These @count posts will be tagged with the term !term.', array('!term' => '<em>' . l($term->name, 'taxonomy/term/' . $term->tid) . '</em>'));
      return confirm_form($form, t('Are you sure you want to tag these posts?'), 'admin/content/masstag', $description, t('Apply tag'), t('Cancel'));
    }
    else {
      drupal_set_message(t('No posts matched the keyword %key.', array('%key' => $form_state['values']['key'])));
    }
  }
  $form['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Keyword or phrase'),
    '#description' => t('Enter a keyword or phrase used to select a set of posts to be tagged.'),
    '#required' => TRUE,
  );
  $form['handling'] = array(
    '#type' => 'radios',
    '#title' => t('Phrase handling'),
    '#options' => array('all' => t('Select posts containing all words in the phrase.'), 'any' => t('Select posts containing any word in the phrase.'), 'exact' => t('Select posts containing the exact phrase.')),
    '#description' => t('Choose the method for handling multiple-word phrases.'),
    '#default_value' => 'all',
    '#required' => TRUE,
  );
  $form['tid'] = array(
    '#type' => 'select',
    '#title' => t('Taxonomy term'),
    '#options' => taxonomy_form_all(TRUE),
    '#description' => t('Select a taxonomy term to apply to all posts which contain the keyword or phrase.'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Tag the given nodes or rebuild the confirmation form.
 */
function masstag_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['confirm'])) {
    $nodes = $form_state['values']['nodes'];
    foreach ($nodes as $nid => $vid) {
      // For speed we do not do a node load and node save operation. That
      // would, however, be the correct way to do this if some custom workflow
      // depended on being notified of changes to node terms!
      $term_node = array('nid' => $nid, 'vid' => $vid, 'tid' => $form_state['values']['term']->tid);
      db_query('DELETE FROM {term_node} WHERE vid = %d AND tid = %d', $term_node['vid'], $term_node['tid']);
      drupal_write_record('term_node', $term_node);
    }
    drupal_set_message(format_plural(count($nodes), 'Tagged 1 post.', 'Tagged @count posts.'));
  }
  else {
    $form_state['rebuild'] = TRUE;
  }
}
